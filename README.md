# git commands

## intro

```
> git add -A
> git status
> git commit -m "message"
> git log
> git push
> git pull # git fetch && git merge
> git clone {url}
```

## branches

```
> git branch new-feature
> git branch
> git checkout new-feature
> git checkout -b new-feature-2 # git branch new-feature && git checkout new-feature
> git checkout - # alternate between last two checked branches
> git merge new-feature
> git branch -d new-feature # delete branch
```

## stash

```
> git stash
> git stash apply
```

## logs

```
> git log
> git log {argument}
> git log --oneline
> git log --graph
> git log --oneline --graph
> git log --decorate # show refrunces
> git log -p # change in each commit
> git log --stat # number of search and deletion of each commit

> git log -3 # show last number of commits
> git log --after="yesterday" # --after="last week" # --after="5 min ago"
> git log --after="" --before=""
> git log --author="Mohamed"
> git log -S"Math" # commit were Math is changes
> git log -p -S"Math"
> git log -i --author="Mohamed" # -i => ingnore!!
> git log --no-merges
> git log app.js # info about app.js
> git log -3 -i --author="trevor" README.md
> git log -S"Math" --after="2 months ago" --oneline --stat
```

## diffs

```
> git diff
> git diff --stat # show in condance way
> git diff --cached # defrances between stage and working
> git diff HEAD # defrances between working and commited
> git diff origin/master
> git diff origin/master getMonthName.js
```

## blame

```
> git blame getMonthName.js
> git log 73cdc43c -p
```

## tag

```
> git tag v1.0.0
> git tag
> git push --tags
> git tag -a v2.0.0 -m "some message"
```

## rebase

```
> git rebase -i origin/master
```

## bisect

```
> git bisect
> git bisect good 105e7CD
> git bisect reset
```

